myRIO Sensor Drivers
====================

This is an assortment of various sensor drivers for the [NI myRIO](http://www.ni.com/myrio/).

This includes drivers for the:

* [Flexiforce Force Sensor](http://www.tekscan.com/flexible-force-sensors)
* [Parallax Compass HMC5883L](http://www.parallax.com/product/29133)
* [Parallax Gyroscope L3G4200D](http://www.parallax.com/product/27911)
* [Parallax Sensirion SHT1x](http://www.parallax.com/product/28018)
* [QTI Sensor](http://www.parallax.com/product/555-27401)
* Two Motor Drive (standard 2 motor drive train)
* [Parallax PING Ultrasonic Sensor](http://www.parallax.com/product/28015)

Downloads
=========

* [myRIO Sensor Drivers v1.0.zip](https://bitbucket.org/edwardcw/myrio-sensor-drivers/downloads/myRIO%20Sensor%20Drivers%20v1.0.zip)

The downloads are compatible with LabVIEW 2010 and above.

Copyright
=========
These drivers are licensed under the [3-clause BSD license](https://pthree.org/2007/08/08/bsd-license-explained-in-layman-terms/).

Copyright © 2014 Edward Wang <edward.c.wang@compdigitec.com>

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the author may be used to endorse nor promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
