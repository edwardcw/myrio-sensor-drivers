﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="13008000">
	<Property Name="NI.Lib.Icon" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">318799872</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Parallax Sensirion SHT1x Calculate Dew Point.vi" Type="VI" URL="../Parallax Sensirion SHT1x Calculate Dew Point.vi"/>
	<Item Name="Parallax Sensirion SHT1x Calculate Humidity Corrected.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Calculate Humidity Corrected.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Calculate Humidity.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Calculate Humidity.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Calculate Temperature.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Calculate Temperature.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Close.vi" Type="VI" URL="../Parallax Sensirion SHT1x Close.vi"/>
	<Item Name="Parallax Sensirion SHT1x Get Measurement Bits.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Get Measurement Bits.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Heater Control.vi" Type="VI" URL="../Parallax Sensirion SHT1x Heater Control.vi"/>
	<Item Name="Parallax Sensirion SHT1x Open.vi" Type="VI" URL="../Parallax Sensirion SHT1x Open.vi"/>
	<Item Name="Parallax Sensirion SHT1x Process ACK Bit.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Process ACK Bit.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Read Bit.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Read Bit.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Read Byte.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Read Byte.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Read Humidity.vi" Type="VI" URL="../Parallax Sensirion SHT1x Read Humidity.vi"/>
	<Item Name="Parallax Sensirion SHT1x Read Raw Data.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Read Raw Data.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Read Status Register.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Read Status Register.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Read Temperature.vi" Type="VI" URL="../Parallax Sensirion SHT1x Read Temperature.vi"/>
	<Item Name="Parallax Sensirion SHT1x Send Bit.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Send Bit.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Send Byte.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Send Byte.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Transmission Start.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Transmission Start.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x Update Voltage.vi" Type="VI" URL="../Parallax Sensirion SHT1x Update Voltage.vi"/>
	<Item Name="Parallax Sensirion SHT1x Write Status Register.vi" Type="VI" URL="../Private/Parallax Sensirion SHT1x Write Status Register.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Parallax Sensirion SHT1x.ctl" Type="VI" URL="../Private/Parallax Sensirion SHT1x.ctl"/>
</Library>
